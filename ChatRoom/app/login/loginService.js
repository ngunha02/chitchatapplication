﻿(function () {
    'use strict';
    angular.module('Auth')
        .factory('Auth', ['$firebaseAuth', authService]);

    function authService($firebaseAuth) {

        var auth = $firebaseAuth();
        return auth;
    }
}());