﻿(function () {
    'use strict';
    angular.module('Auth')
        .controller('authCtrl', ['Auth', '$state', authCtrl]);

    function authCtrl(Auth, $state) {
        var vm = this;
        vm.user = {
            email: '',
            password: ''
        }
        vm.login = function () {
            Auth.$signInWithEmailAndPassword(vm.user.email, vm.user.password).then(function (auth) {
                $state.go('home');
            }, function (error) {
                vm.error = error;
            });
        }

        vm.register = function () {
            Auth.$createUserWithEmailAndPassword(vm.user.email, vm.user.password).then(function (user) {
                vm.login();
            }, function (error) {
                vm.error = error;
            });
        }
    }
}())