﻿(function () {
    'use strict';
    

    angular.module('app', ['firebase', 'ui.router', 'Auth', 'user', 'rooms', 'angular-md5'])
        .directive("outsideClick", ['$document', '$parse', function ($document, $parse) {
        return {
            link: function ($scope, $element, $attributes) {
                var scopeExpression = $attributes.outsideClick,
                  onDocumentClick = function (event) {
                      // check for flag
                      if (!$scope.closeFlag) {
                          $scope.closeFlag = true;
                          return;
                      }
                      var parent = event.target;
                      while (parent && parent !== $element[0]) {
                          parent = parent.parentNode;
                      }
                      if (!parent) {
                          $scope.$apply(scopeExpression);
                      }
                  }
                $document.on("click", onDocumentClick);
                $element.on('$destroy', function () {
                    $document.off("click", onDocumentClick);
                });
            }
        }
    }]);
}())