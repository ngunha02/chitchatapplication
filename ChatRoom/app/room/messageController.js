﻿(function () {

    'use strict';
    angular.module('rooms')
        .controller('messageCtrl', ['messages', 'channelName', 'profile', 'user', '$window', '$scope',  messageCtrl])

    function messageCtrl(messages, channelName, profile, user, $window, $scope) {
        var vm = this;
        vm.messages = messages;
        vm.channelName = channelName;
        vm.message = '';
        vm.profile = profile;

        vm.participants = [];
        vm.showParticipant = false;
        vm.displayParticipants = function () {
            $window.onclick = null;
            for (var i = 0 ; i < vm.messages.length; i++) {
                for (var key in vm.messages[i]) {
                    var name = user.getDisplayName(vm.messages[i]['uid']);
                    if (vm.participants.indexOf(name) === -1) {
                        vm.participants.push(name);
                    }
                }
            }
            vm.showParticipant = true;
            if (vm.showParticipant) {
                $window.onclick = function (event) {
                    vm.showParticipant = false;
                }
            }
        }

        vm.getDisplayName = function (id) {
            if (user.getDisplayName(id) === null || user.getDisplayName(id) === undefined){
                return "<user> has been deleted";
            }
            return user.getDisplayName(id);
        }

        vm.sendMessage = function () {
            if (vm.message.length > 0) {
                vm.messages.$add({
                    uid: profile.$id,
                    body: vm.message,
                    timestamp: firebase.database.ServerValue.TIMESTAMP
                }).then(function () {
                    vm.message = '';
                });
            }
        };

        vm.searchMessage = function () {

        }
    }

}())