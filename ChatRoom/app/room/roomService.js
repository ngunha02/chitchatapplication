﻿(function () {

    'use strict';
    angular.module('rooms')
        .factory('rooms', ['$firebaseArray', rooms]);

    function rooms($firebaseArray) {
        var ref = firebase.database().ref('rooms');
        var rooms = $firebaseArray(ref);
        
        var messagesRef = firebase.database().ref('roomMessages');
        var messagesInfo = $firebaseArray(messagesRef);
        //return rooms;

        return {
            rooms: function () {
                return rooms;
            },
            messagesInfo: function () {
                return messagesInfo;
            }
        }
    }
}());