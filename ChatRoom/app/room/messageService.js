﻿(function () {
    'use strict';
    angular.module('rooms')
        .factory('messages', ['$firebaseArray', messages]);

    function messages($firebaseArray) {
        var roomMessagesRef = firebase.database().ref('roomMessages');
        var userMessagesRef = firebase.database().ref('userMessages')
        return {
            forRoom: function (roomId) {
                return $firebaseArray(roomMessagesRef.child(roomId));
            },
            forUsers: function (uid1, uid2) {
                var path = uid1 < uid2 ? uid1 + '/' + uid2 : uid2 + '/' + uid1;

                return $firebaseArray(userMessagesRef.child(path));
            }
        };
    }

}())