﻿(function () {
    'use strict';
    angular.module('rooms')
        .controller('roomCtrl', ['channels', 'profile', '$state', 'Auth', 'user', 'messages', 'rooms', roomCtrl]);

    function roomCtrl(channels, profile, $state, Auth, user, messages, rooms) {
        var vm = this;
        vm.profile = profile;
        vm.rooms =  channels;
        vm.getDisplayName = user.getDisplayName;
        vm.getGravatar = user.getGravatar;
        vm.newRoom = {
            name: ''
        };
        vm.users = user.all;
        vm.messageHistory = '';
        vm.histories = [];
        vm.visibleAdmin = false;
        if (typeof vm.profile.isAdmin === 'undefined') {
            vm.visibleAdmin = false;
        }
        else {
            vm.visibleAdmin = true;
        }
        vm.onEnter = function (keyEvent) {
            if (keyEvent.which === 13) {
                if (vm.messageHistory === '') {
                    vm.histories = [];
                    return;
                }
                vm.histories = [];
                rooms.messagesInfo().$loaded().then(function (data) {        
                    for (var i = 0; i < data.length; i++) {
                        for (var key in data[i]) {
                            if (data[i][key].body === undefined) {
                                break;
                            }
                            if (data[i][key].body.indexOf(vm.messageHistory) !== -1) {
                                vm.histories.push(data[i][key].body);
                            }
                        }                     
                    }
                }).catch(function (error) {

                });
            }
        }
        vm.removeUser = function (id) {
            var idToBeBlocked = id;
            user.delUser(idToBeBlocked);
        }
        
        vm.createRoom = function () {
            vm.rooms.$add(vm.newRoom).then(function (ref) {
                $state.go('channels.messages', { roomId: ref.key });
                vm.newRoom = {
                    name: ''
                };
            }, function (error) {

            });
        };
        user.setOnline(profile.$id);
        vm.logout = function () {
            vm.profile.online = null;
            vm.profile.$save().then(function () {
                Auth.$signOut();
                $state.go('home');
            });
        };
    }

}())