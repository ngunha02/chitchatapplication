﻿(function () {

    'use strict';
    angular.module('common', [])
        .constant('appSetting',
        {
            FirebaseUrl: 'https://chatroom-54053.firebaseio.com/'
        });

}())