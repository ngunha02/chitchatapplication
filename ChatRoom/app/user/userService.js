﻿(function () {
    'use strict';
    angular.module('user')
            .factory('user', ['$firebaseArray', '$firebaseObject', 'Auth', userService])

    function userService($firebaseArray, $firebaseObject, Auth) {
        var usersRef = firebase.database().ref().child('users');
        var connectedRef = firebase.database().ref('.info/connected');
        var users = $firebaseArray(usersRef);
        var user = {
            getProfile: function (uid) {
                return $firebaseObject(usersRef.child(uid));
            },
            getDisplayName: function (uid) {
                if (users.$getRecord(uid) === null){
                    return "USER HAS BEEN DELETED";
                }
                return users.$getRecord(uid).displayName;
            },
            all: users,
            getGravatar: function (uid) {
                return '//www.gravatar.com/avatar/' + users.$getRecord(uid).emailHash;
            },
            delUser: function (uid) {
                var user = users.$getRecord(uid); 
                var online = $firebaseArray(usersRef.child(uid + '/isBlocked'));
                if (user !== null) {
                    online.$add(true).then(function () {
                        users.$getRecord(uid).displayName = users.$getRecord(uid).displayName + " <BLOCKED>";                     
                        users.$save(users.$getRecord(uid)).then(function (ref) {
                            console.log('saved');
                        }, function(error) {
                            console.log("Error:", error);
                        });
                    }, function (error) {
                        console.log('erro ' + JSON.stringify(error));
                    });
                }
            }
            , setOnline: function (uid) {
                var connected = $firebaseObject(connectedRef);
                var online = $firebaseArray(usersRef.child(uid + '/online'));
                connected.$watch(function () {
                    if (connected.$value === true) {
                        online.$add(true).then(function (connectedRef) {
                            connectedRef.onDisconnect().remove();
                        });
                    }
                });
            }
        };
        return user;
    }

}())