﻿(function () {
    'use strict';
    angular.module('user')
        .controller('userCtrl', ['auth', 'profile', '$state', 'md5', userCtrl]);

    function userCtrl(auth, profile, $state, md5) {
        var vm = this;
        vm.profile = profile;
        vm.updateProfile = function () {
            vm.profile.emailHash = md5.createHash(auth.email);
            vm.profile.$save().then(function () {
                $state.go('channels');
            });
        };
    }


}())