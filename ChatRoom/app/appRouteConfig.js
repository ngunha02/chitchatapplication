﻿(function () {

    'use strict';
    angular.module('app')
        .config(['$stateProvider', '$urlRouterProvider',
                function ($stateProvider, $urlRouterProvider) {
                    var config = {
                        apiKey: "AIzaSyDif4TEyLq0mPuRpQN7fNcQNQaZMHM7bPo",
                        authDomain: "chatroom-54053.firebaseapp.com",
                        databaseURL: "https://chatroom-54053.firebaseio.com",
                        storageBucket: "chatroom-54053.appspot.com",
                        messagingSenderId: "532721705126"
                    };
                    firebase.initializeApp(config);
                    $stateProvider
                        .state('home', {
                            url: '/',
                            templateUrl: 'app/home/home.html'
                            , resolve: {                        
                                requireNoAuth: function ($state, Auth) {
                                    return Auth.$requireSignIn().then(function (auth) {
                                        $state.go('channels');
                                    }, function (error) {
                                        return;
                                    });
                                }
                            }
                        })
                        .state('login', {
                            url: '/login',
                            templateUrl: 'app/login/login.html',
                            controller: 'authCtrl as vm'
                            ,resolve: {
                                requireNoAuth: function ($state, Auth) {
                                    return Auth.$requireSignIn().then(function (auth) {
                                        $state.go('channels');
                                    }, function (error) {
                                        return;
                                    });
                                }
                            }
                        })
                        .state('register', {
                            url: '/register',
                            templateUrl: 'app/login/register.html',
                            controller: 'authCtrl as vm'
                            ,resolve: {
                                requireNoAuth: function ($state, Auth) {
                                    return Auth.$requireSignIn().then(function (auth) {
                                        $state.go('home');
                                    }, function (error) {
                                        return;
                                    });
                                }
                            }
                        })
                        .state('profile', {
                            url: '/profile',
                            templateUrl: 'app/user/profile.html',
                            controller: 'userCtrl as vm',
                            resolve: {
                                auth: function ($state, user, Auth) {
                                    return Auth.$requireSignIn().catch(function () {
                                        $state.go('home');
                                    });
                                },
                                profile: function (user, Auth) {
                                    return Auth.$requireSignIn().then(function (auth) {
                                        return user.getProfile(auth.uid).$loaded();
                                    });
                                }
                            }
                        })
                        .state('blocked', {
                            url: '/blocked',
                            templateUrl: 'app/room/blocked.html'
                        })
                        .state('channels', {
                            url: '/channels',
                            templateUrl: 'app/room/room.html',
                            controller: 'roomCtrl as vm',
                            resolve: {
                                profile: function ($state, Auth, user) {
                                    return Auth.$requireSignIn().then(function (auth) {
                                        return user.getProfile(auth.uid).$loaded().then(function (profile) {
                                            if (profile.displayName) {
                                                if (typeof profile.isBlocked !== 'undefined') {
                                                    Auth.$signOut();
                                                    $state.go('blocked');
                                                }
                                                return profile;
                                            } else {
                                                //already blocked!!
                                                $state.go('profile');
                                            }
                                        });
                                    }, function (error) {
                                        $state.go('home');
                                    });
                                },
                                channels: function ($firebaseArray, rooms) {
                                    return rooms.rooms().$loaded().then(function (rooms) {
                                        return rooms;
                                    }, function (error) {
                                        console.log('error ' + JSON.stringify(error));
                                    })
                                }
                                
                            }
                        })
                        .state('channels.create', {
                            url: '/create',
                            templateUrl: 'app/room/create.html',
                            controller: 'roomCtrl as vm'
                        })
                        .state('channels.messages', {
                            url: '/{roomId}/messages',
                            templateUrl: 'app/room/message.html',
                            controller: 'messageCtrl as vm',
                            resolve: {
                                messages: function ($stateParams, messages) {
                                    return messages.forRoom($stateParams.roomId).$loaded();
                                },
                                //channelName: function ($stateParams, rooms) {
                                //    return '#' + rooms.$getRecord($stateParams.roomId).name;
                                //}
                                channelName: function ($stateParams, rooms) {
                                    return '#' + rooms.rooms().$getRecord($stateParams.roomId).name;
                                }
                            }
                        })
                        .state('channels.direct', {
                            url: '/{uid}/messages/direct',
                            templateUrl: 'app/room/message.html',
                            controller: 'messageCtrl as vm',
                            resolve: {
                                messages: function ($stateParams, messages, profile) {
                                    return messages.forUsers($stateParams.uid, profile.$id).$loaded();
                                },
                                channelName: function ($stateParams, user) {
                                    return user.all.$loaded().then(function () {
                                        return '@' + user.getDisplayName($stateParams.uid);
                                    });
                                }
                            }
                        });

                    $urlRouterProvider.otherwise('/');
                }
        ])

}());